#!/usr/bin/python 

from appnet import *
import sys
import socket
import messages_pb2
import unittest
import threading

# ******************************************************************************************************************

class TestServerReg(unittest.TestCase):
    def setUp(self):
        self.sockobj = make_socket()

    def tearDown(self):
        self.sockobj.close()

    def test_reg(self):
        msg = register_request(sock=self.sockobj, name='AAA', passw='123')
        self.assertEqual(msg.reg_response.status, 1)

class TestServerInput(unittest.TestCase):
    def setUp(self):
        self.sockobj = make_socket()

    def tearDown(self):
        self.sockobj.close()

    def test_input(self):
        name = 'AAA'
        passw = '123'

        msg = input_request(sock=self.sockobj, name='AAA', passw='123')
        self.assertEqual(msg.input_response.status, 1)

    def test_join(self):
        name = 'AAA'
        passw = '123'

        msg = input_request(sock=self.sockobj, name='AAA', passw='123')
        self.assertEqual(msg.input_response.status, 1)

class TestServerHistory(unittest.TestCase):
    def setUp(self):
        self.sockobj = make_socket()

    def tearDown(self):
        self.sockobj.close()

    def test_join(self):
        name = 'AAA'
        passw = '123'
        channel = 'aaa'

        msg = input_request(sock=self.sockobj, name=name, passw='123')
        self.assertEqual(msg.input_response.status, 1)

        msg = join_channel_request(sock=self.sockobj, client_id=msg.input_response.client_id, channel_name=channel)
        self.assertEqual(msg.join_room_response.status, 1)
        self.assertEqual(msg.join_room_response.channel_name, channel)

    def test_emptyHistory(self):
        name = 'AAA'
        passw = '123'
        channel = 'aaaa'

        msg = input_request(sock=self.sockobj, name=name, passw='123')
        self.assertEqual(msg.input_response.status, 1)

        client_id = msg.input_response.client_id
        msg = join_channel_request(sock=self.sockobj, client_id=msg.input_response.client_id, channel_name=channel)
        self.assertEqual(msg.join_room_response.status, 1)
        self.assertEqual(msg.join_room_response.channel_name, channel)

        msg = history_request(sock=self.sockobj, client_id=client_id, channel_name=channel)
        self.assertEqual(msg.history_response.channel_name, channel)
        self.assertEqual(len(msg.history_response.messages), 0)

class TestServerSimpleDialog(unittest.TestCase):
    def setUp(self):
        self.sockobj = make_socket()

    def tearDown(self):
        self.sockobj.close()

    def test_sendOneMsg(self):
        name = 'AAA'
        passw = '123'
        channel = 'aaa1'
        test_msg = "first msg"

        msg = input_request(sock=self.sockobj, name=name, passw='123')
        self.assertEqual(msg.input_response.status, 1)

        client_id = msg.input_response.client_id

        msg = join_channel_request(sock=self.sockobj, client_id=msg.input_response.client_id, channel_name=channel)
        self.assertEqual(msg.join_room_response.status, 1)
        self.assertEqual(msg.join_room_response.channel_name, channel)

        room_id = msg.join_room_response.room_id

        msg = history_request(sock=self.sockobj, client_id=client_id, channel_name=channel)
        self.assertEqual(msg.history_response.channel_name, channel)
        self.assertEqual(len(msg.history_response.messages), 0)

        res = text_request(sock=self.sockobj, login=name, room_id=room_id , channel_name=channel, text=test_msg)
        self.assertEqual(res.text_response.login, name)
        self.assertEqual(res.text_response.room_id, room_id)
        self.assertEqual(res.text_response.channel_name, channel)
        self.assertEqual(res.text_response.text, test_msg)

    def test_historyOneMsg(self):
        name = 'AAA'
        passw = '123'
        channel = 'aaa2'
        test_msg = "first msg"

        msg = input_request(sock=self.sockobj, name=name, passw='123')
        self.assertEqual(msg.input_response.status, 1)

        client_id = msg.input_response.client_id

        msg = join_channel_request(sock=self.sockobj, client_id=msg.input_response.client_id, channel_name=channel)

        room_id = msg.join_room_response.room_id
        self.assertEqual(msg.join_room_response.status, 1)
        self.assertEqual(msg.join_room_response.channel_name, channel)

        res = text_request(sock=self.sockobj, login=name, room_id=room_id , channel_name=channel, text=test_msg)
        self.assertEqual(res.text_response.login, name)
        self.assertEqual(res.text_response.room_id, room_id)
        self.assertEqual(res.text_response.channel_name, channel)
        self.assertEqual(res.text_response.text, test_msg)
        
        res = history_request(sock=self.sockobj, client_id=client_id, channel_name=channel)
        self.assertEqual(res.history_response.channel_name, channel)
        self.assertEqual(len(res.history_response.messages), 1)

        self.assertEqual(res.history_response.messages[0].login, name)
        self.assertEqual(res.history_response.messages[0].room_id, room_id)
        self.assertEqual(res.history_response.messages[0].channel_name, channel)
        self.assertEqual(res.history_response.messages[0].text, test_msg)


        res = history_request(sock=self.sockobj, client_id=client_id, channel_name=channel)
        self.assertEqual(res.history_response.channel_name, channel)
        self.assertEqual(len(res.history_response.messages), 1)

        self.assertEqual(res.history_response.messages[0].login, name)
        self.assertEqual(res.history_response.messages[0].room_id, room_id)
        self.assertEqual(res.history_response.messages[0].channel_name, channel)
        self.assertEqual(res.history_response.messages[0].text, test_msg)

class TestServerTwoPersonDialog(unittest.TestCase):
    def setUp(self):
        self.sockobj_first = make_socket()
        self.name_first = 'AAA'

        self.sockobj_second = make_socket()
        self.name_second = 'BBB'
        
        self.passw = '123'
        self.channel = 'aaa2'

    def tearDown(self):
        self.sockobj_first.close()
        self.sockobj_second.close()

    def test_sendOneMsg(self):
        msg = input_request(sock=self.sockobj_one, name=name_one, passw=passw)
        self.assertEqual(msg.input_response.status, 1)
        msg = join_channel_request(sock=self.sockobj_one, client_id=msg.input_response.client_id, channel_name=channel)
        self.assertEqual(msg.join_room_response.status, 1)
        self.assertEqual(msg.join_room_response.channel_name, channel)

        client_id = msg.input_response.client_id

        room_id = msg.join_room_response.room_id

        msg = history_request(sock=self.sockobj, client_id=client_id, channel_name=channel)
        self.assertEqual(msg.history_response.channel_name, channel)
        self.assertEqual(len(msg.history_response.messages), 0)

        
        res = text_request(sock=self.sockobj, login=name, room_id=room_id , channel_name=channel, text=test_msg)
        self.assertEqual(res.text_response.login, name)
        self.assertEqual(res.text_response.room_id, room_id)
        self.assertEqual(res.text_response.channel_name, channel)
        self.assertEqual(res.text_response.text, test_msg)
        
# ******************************************************************************************************************

if __name__ == '__main__':
    #unittest.main()
    pass


