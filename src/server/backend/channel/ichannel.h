#ifndef ICHANNEL_H
#define ICHANNEL_H

#include <memory>
#include <deque>

#include "backend/ServiceConfig.h"

class IChannel {
public:
    virtual  void join_channel(const Config::JoinChannelConfig &) = 0;

    virtual void leave(identifier_t client_id) = 0;

    virtual void notification(const msg_text_t&) = 0;
    
    virtual std::string get_channel_name() const = 0;

    virtual std::deque<identifier_t> get_subscribers() const = 0;

    virtual std::deque<msg_text_t> get_history() const = 0;

    virtual void send_history(const Config::HistoryChannelConfig &) = 0;

    virtual ~IChannel() = default;
};

using channel_ptr_t = std::shared_ptr<IChannel>;

#endif // ICHANNEL
