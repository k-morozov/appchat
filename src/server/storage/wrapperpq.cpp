#include "wrapperpq.h"
#include <iostream>
#include <utility>
#include <string>
#include <cstdlib>
#include <filesystem>
#include <boost/log/trivial.hpp>

#include <stdexcept>

namespace Storage
{

bool WrapperPQ::connect(const ConnectSettings& setting) {
    bool status = true;
    if (!isConnect) {
        conn = PQconnectdb(std::string("user=" + setting.user
                                       + " password=" + setting.password
                                       + " host=" + setting.host
                                       + " dbname=" + setting.db
                                       ).c_str());

        if (PQstatus(conn) != CONNECTION_OK) {
            std::cerr << "connect()" << std::endl;
            PQerrorMessage(conn);
            status = false;
        }
    }

    return isConnect = status;
}

bool WrapperPQ::add_logins(std::string login, std::string password) {
    bool status = true;
    if (isConnect) {
        PGresult *res;
        res = PQexec(conn, std::string("insert into clients (login, password) values ('" + login + "', '" + password + "');").c_str());
        if (PQresultStatus(res) != PGRES_COMMAND_OK) {
            std::cerr << "add_login: " << PQresultErrorMessage(res) << std::endl;
            PQclear(res);
            status = false;
        } else {
            PQclear(res);
        }
    } else {
        std::cerr << "No connect" << std::endl;
        status = false;
    }
    return isConnect = status;
}

identifier_t WrapperPQ::get_loginid(std::string login) const {
    identifier_t result = 0;
    if (isConnect) {
        PGresult *res;
        res = PQexec(conn, std::string("select id from clients where login='" + login + "';").c_str());
        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            std::cerr << "add_login: " << PQresultErrorMessage(res) << std::endl;
            PQclear(res);
        } else {
            result = PQntuples(res) ? std::stoi(PQgetvalue(res, 0, 0)) : 0;
            PQclear(res);
        }
    } else {
        std::cerr << "No connect" << std::endl;
    }
    return result;
}

identifier_t  WrapperPQ::check_client(std::string login, std::string password) const {
    identifier_t result = 0;
    if (isConnect) {
        PGresult *res;
        res = PQexec(conn, std::string("select id from clients where login='" + login + "' and password='" + password + "';").c_str());
        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            std::cerr << "add_login: " << PQresultErrorMessage(res) << std::endl;
            PQclear(res);
        } else {
            result = PQntuples(res) ? std::stoi(PQgetvalue(res, 0, 0)) : 0;
            PQclear(res);
        }
    } else {
        std::cerr << "No connect" << std::endl;
    }
    return result;
}

bool WrapperPQ::save_text_message(const msg_text_t& message) {
    bool status = true;
    if (isConnect) {
        PGresult *res;
        std::stringstream ss;
        ss << std::setfill('0') << std::setw(4) << message.dt.date.year << "-"
            << std::setfill('0') << std::setw(2) << message.dt.date.month << "-"
            << std::setfill('0') << std::setw(2) << message.dt.date.day << " "
            << std::setfill('0') << std::setw(2) << message.dt.time.hours << ":"
            << std::setfill('0') << std::setw(2) << message.dt.time.minutes << ":"
            << std::setfill('0') << std::setw(2) << message.dt.time.seconds;
        res = PQexec(conn, std::string(
                         "insert into history(client_id, channel_name, datetime, message) "
                         "values (" + std::to_string(get_loginid(message.author)) + ", '"
                         + message.channel_name + "', '"
                         + ss.str() + "', '"
                         + message.text + "');"
                         ).c_str());
        if (PQresultStatus(res) != PGRES_COMMAND_OK) {
            std::cerr << "save msg: " << PQresultErrorMessage(res) << std::endl;
            PQclear(res);
            status = false;
        } else {
            PQclear(res);
        }
    } else {
        std::cerr << "No connect" << std::endl;
        status = false;
    }
    return isConnect = status;

}

std::deque<msg_text_t> WrapperPQ::get_history(const std::string& a_channel_name) const {
    std::deque<msg_text_t> history_room;
    if (isConnect) {
        PGresult *res;
        res = PQexec(conn, std::string("select client_id, datetime, message from history where channel_name='" 
            + a_channel_name + "';").c_str());
        if (PQresultStatus(res) == PGRES_TUPLES_OK) {

            for (int i = 0; i < PQntuples(res); i++) {
                msg_text_t msg;
                msg.author = get_login(PQgetvalue(res, i, 0));
                msg.channel_name = a_channel_name; 
                std::string dt = PQgetvalue(res, i, 1);
                msg.text = PQgetvalue(res, i, 2);

                std::istringstream iss(dt);
                std::tm tm;
                iss >> std::get_time(&tm, "%Y-%m-%d %H:%M:%S");
                msg.dt = tm;

                BOOST_LOG_TRIVIAL(info) << "DB: " << msg.author << " " << msg.channel_name << " "
                    << msg.dt.date.year << "." << msg.dt.date.month << "." << msg.dt.date.day << " "
                    << msg.dt.time.hours << ":" << msg.dt.time.minutes << " " << msg.text;

                history_room.push_back(std::move(msg));
            }

            PQclear(res);
        } else {
            std::cerr << "print_history: " << PQresultErrorMessage(res) << std::endl;
            PQclear(res);
        }
    } else {
        std::cerr << "No connect" << std::endl;
    }

    return history_room;
}

std::string WrapperPQ::get_login(const std::string& client_id) const {
    std::string result = "unknown";
    if (isConnect) {
        PGresult *res;
        res = PQexec(conn, std::string("select login from clients where id='" 
            + client_id + "';").c_str());
        if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            if (PQntuples(res) == 1) {
                result = PQgetvalue(res, 0, 0); 
            }
        }

        PQclear(res);
    } else {
        std::cerr << "No connect" << std::endl;
    }
    return result;
}

std::deque<std::string> WrapperPQ::get_channels(identifier_t client_id) const {
    std::deque<std::string> result;

    if (!isConnect) {
        std::cerr << "No connect" << std::endl;
        return result;
    }

    PGresult* res;

    const std::string query =
        "SELECT DISTINCT channel_name FROM history WHERE client_id = " + std::to_string(client_id);

    res = PQexec(conn, query.c_str());

    if (PQresultStatus(res) == PGRES_TUPLES_OK) {
        for (int i = 0; i < PQntuples(res); i++) {
            result.push_back(PQgetvalue(res, i, 0));
        }
    }

    PQclear(res);

    return result;
}

WrapperPQ::~WrapperPQ() {
    PQfinish(conn);
}

} // namespace Storage
