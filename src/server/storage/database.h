#pragma once

#include <deque>
#include <memory>

#include "settings.h"
#include "protocol/protocol.h"

namespace Storage
{


class Database
{
public:
    virtual ~Database() {

    }

    // @todo fixme
    virtual bool connect(const ConnectSettings& setting = ConnectSettings()) = 0;

    virtual bool save_text_message(const msg_text_t& message) = 0;
  
    virtual std::deque<msg_text_t> get_history(const std::string& a_channel_name) const = 0;

    virtual bool add_logins(std::string login, std::string password) = 0;

    virtual identifier_t get_loginid(std::string login) const = 0;

    virtual identifier_t check_client(std::string login, std::string password) const = 0;

    virtual std::deque<std::string> get_channels(identifier_t client_id) const = 0;

};

using database_ptr = std::shared_ptr<Database>;

} //namespace Storage
